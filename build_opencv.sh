#!/bin/bash

apt-get -y update

apt-get -y install python2.7-dev wget unzip \
                   build-essential cmake git pkg-config libatlas-base-dev gfortran \
                   libjasper-dev libgtk2.0-dev libavcodec-dev libavformat-dev \
                   libswscale-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev
wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py
pip install numpy matplotlib

wget https://github.com/Itseez/opencv/archive/3.1.0.zip -O opencv3.zip && \
unzip -q opencv3.zip && mv /opencv-3.1.0 /opencv

wget https://github.com/Itseez/opencv_contrib/archive/3.1.0.zip -O opencv_contrib3.zip && \
unzip -q opencv_contrib3.zip && mv /opencv_contrib-3.1.0 /opencv_contrib

mkdir /opencv/build
cd /opencv/build

cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D BUILD_PYTHON_SUPPORT=ON \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_C_EXAMPLES=OFF \
	-D INSTALL_PYTHON_EXAMPLES=OFF \
	-D OPENCV_EXTRA_MODULES_PATH=/opencv_contrib/modules \
	-D BUILD_EXAMPLES=OFF \
	-D BUILD_NEW_PYTHON_SUPPORT=ON \
	-D WITH_IPP=OFF \
	-D WITH_V4L=OFF ..

make -j2

make install

ldconfig

cd /
rm -rf /opencv /opencv_contrib
