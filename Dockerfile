FROM ubuntu:16.04

#3.4.3
ENV PYTHON_VERSION 2.7
ENV NUM_CORES 2

# Install OpenCV 3.0
ADD	build_opencv.sh	/build_opencv.sh
RUN	/bin/sh /build_opencv.sh

# Define default command.
CMD ["bash"]
